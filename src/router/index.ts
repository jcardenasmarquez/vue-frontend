import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Mainpage from "../Pages/Mainpage.vue";
import ControlView from "../Pages/ControlView.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    component: Mainpage,
    children: [],
  },
  {
    path: "/control",
    component: ControlView,
    children: [],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
